
# spa/route53
resource "aws_route53_record" "apex" {
  zone_id = var.zone_id

  name = var.name
  type = "A"

  alias {
    name                   = var.alias.name
    zone_id                = var.alias.zone_id
    evaluate_target_health = true
  }

  lifecycle {
    create_before_destroy = true
  }
}
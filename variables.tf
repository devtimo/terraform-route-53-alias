variable "zone_id" {
  description = "id da zona do dominio"
}

variable "name" {
  description = "Nome do registro"
}

variable "alias" {
  description = "Alias do registro"
  type = object({
    name = string
    zone_id = string
  })
}
